package com.example.demo.controller;

import com.example.demo.model.Movie;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class BasicController {

    @Autowired
    private MovieService movieService;

    @RequestMapping("/")
    String inicio() {
        return "index";
    }

    @RequestMapping("/add")
    String add() {
        return "addMovie";
    }


    @PostMapping("/add")
    String add1(@RequestParam String movie_name, @RequestParam String url, Model model) {
        Movie m = new Movie();
        m.setMovie_name(movie_name);
        m.setUrl(url);
        movieService.addMovie(m);
        model.addAttribute("movies", m);
        model.addAttribute("message", "La película '" + movie_name + "' ha sido añadida.");

        return "actualizacion";
    }

    @RequestMapping("/delete")
    String delete() {
        return "deletemovie";
    }

    @PostMapping("/delete")
    String delete1(@RequestParam Long id, Model model) {

        Movie m = movieService.getMovieById(id);
        if (m == null) {
            model.addAttribute("message", "La película con id '" + id + "' no existe.");
            return "index";
        } else {
            String nombre = m.getMovie_name();
            movieService.deleteMovie(id);
            model.addAttribute("movies", m);
            model.addAttribute("message", "La película '" + nombre + "' ha sido eliminada.");
            return "actualizacion";
        }

    }


    @RequestMapping("/update")
    String modify() {
        return "modifymovie";
    }

    @PostMapping("/update")
    String modify1(@RequestParam Long id, @RequestParam String movie_name, @RequestParam String url, Model model) {

        Movie m = movieService.getMovieById(id);

        if (m == null) {
            model.addAttribute("message", "La película con id '" + id + "' no existe.");
            return "index";
        } else {
            m.setMovie_name(movie_name);
            m.setUrl(url);
            movieService.updateMovie(id, m);
            model.addAttribute("movies", m);
            model.addAttribute("message", "La película '" + movie_name + "' ha sido actualizada.");
            return "actualizacion";
        }
    }


    @RequestMapping("/listado")
    String getAllMovies() {
        return "listadopeliculas";
    }

    @GetMapping("/listado")
    String getAllMovies(Model model) {
        List<Movie> movies = movieService.getAllMovies();
        model.addAttribute("movies", movies);
        return "listadopeliculas";
    }

    @PostMapping("/moviebyid")
    String getMovieById(@RequestParam Long id, Model model) {
        Movie movie = movieService.getMovieById(id);
        if (movie != null) {
            model.addAttribute("movies", movie);
        } else {
            model.addAttribute("message", "La película con id: '" + id + "' no existe");
            return "index";
        }
        return "index";
    }

}